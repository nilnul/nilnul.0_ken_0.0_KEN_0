﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="_Note.ascx.cs" Inherits="nilnul.knowledge._Ken._web.learn._Note" %>
<style type="text/css">
* {
	font-size: 100%;
}
</style>

<p>
	发信人: Cracker (rock), 信区: Mathematics<br />
	标 &nbsp;题: Vakil主页上的一段话<br />
	发信站: 水木社区 (Mon Aug 19 19:08:22 2013), 站内<br />
	<br />
	我觉得很有道理，特别是最后一句很逗...<br />
	<br />
	Here&#39;s a phenomenon I was surprised to find: you&#39;ll go to talks, and hear various words, whose definitions you&#39;re not so sure about. At some point you&#39;ll be able to make a sentence using those words; you won&#39;t know what the words mean, but you&#39;ll know the sentence is correct. You&#39;ll also be able to ask a question using those words. You still won&#39;t know what the words mean, but you&#39;ll know the question is interesting, and you&#39;ll want to know the answer. Then later on, you&#39;ll learn what the words mean more precisely, and your sense of how they fit together will make that learning much easier. The reason for this phenomenon is that mathematics is so rich and infinite that it is impossible to learn it systematically, and if you wait to master one topic before moving on to the next, you&#39;ll never get anywhere. Instead, you&#39;ll have tendrils of knowledge extending far from your comfort zone. Then you can later backfill from these tendrils, and extend your comfort zone; this is much easier to 
	do than learning &quot;forwards&quot;. (Caution: this backfilling is necessary. There can be a temptation to learn lots of fancy words and to use them in fancy sentences without being able to say precisely what you mean. You should feel free to do that, but you should always feel a pang of guilt when you do.)<br />
</p>

